import os
import pickle
import numpy as np
import enum
from typing import Any


class ModelType(enum.Enum):
  RANDOM_FOREST = 'random_forest_model.ml'
  NAIVE_BAYES = 'bayes_model.ml'
  SUPPORT_VECTOR = 'svm_model.ml'
  K_NEIGHBORS = 'knn_model.ml'
  LSTM = 'lstm_model.ml'


class SittingOrNotOracle:

  def __init__(self, model_type: ModelType = ModelType.RANDOM_FOREST) -> None:
    self.model_type = model_type
    self.model = self._load_ml_model(self.model_type.value)

  def _load_ml_model(self, model_file_name: str) -> Any:
    model_path = os.path.join(os.path.dirname(__file__), f'ml_models/{model_file_name}')

    with open(model_path, 'rb') as f:
      return pickle.load(f)

  def predict(self, data: np.ndarray) -> np.ndarray:
    classical_models = [
      ModelType.RANDOM_FOREST, 
      ModelType.SUPPORT_VECTOR, 
      ModelType.K_NEIGHBORS, 
      ModelType.NAIVE_BAYES
    ]

    if self.model_type in classical_models:
      data = data.reshape((data.shape[0], data.shape[1] * data.shape[2]))
    
    return self.model.predict(data)


def main():
  oracle = SittingOrNotOracle()

  data = np.arange(12000)
  data = data.reshape(2, 1000, 6)

  result = oracle.predict(data)
  print(result)

if __name__ == '__main__':
  main()